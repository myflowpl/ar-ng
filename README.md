# Migracja na nx

```bash
npm i jsonwebtoken class-validator @nestjs/swagger typeorm @nestjs/typeorm class-transformer @types/express @types/multer
```

# Zadanie

1. Tworzymy `user` module
2. Tworzymy `user-list` component w module `user`
3. Tworzymy `user.service` w module `user`
4. W API dodać routing `user` zwracajacy wszystkich userów
   1. Obecnie ten routing zwraca info o zalogowanym userze
   2. Więc zmieniamy ten routing na `user/me`
5. Wyswietlamy listę uzytkownikow
   1. ustawiamy routing
   2. dodajemy pozycje `user` w menu glownym
   3. dodjemy metode `getUsers` w serwisie
   4. w componencie `user-list` tworzymy stream userwo
   5. w HTML `user-list` wyswietlamy w pętli rekordy
6. Dodajemy sub routing dla `/user/login`
7. Tworzymy komponent `user-login`
8. Konfigurujemy rougint zeby wyswietlic lgowanie







# Repo

> https://bitbucket.org/myflowpl/ar-ng/src

db 

https://bitbucket.org/myflowpl/course-angular/src/init/api/

# ArNg

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.0.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
