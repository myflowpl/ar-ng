import { Component, OnInit } from '@angular/core';
import { Coords, Layer } from '../../map/map.component';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private route: ActivatedRoute) {
    this.route.queryParams.subscribe(params => console.log('params', params));
  }

  point: Coords = {
    lat: 51.505,
    lng: -0.09
  };
  layer = Layer.MAP;

  onEdit(data) {
    console.log('coordsa change', data);
    this.point = data;
  }

  setPoland() {
    this.point = {
      lat: 51.25,
      lng: 22.60,
    };
  }
  setLondek() {
    this.point = {
      lat: 51.505,
      lng: -0.09
    };
  }
  setMap(e) {
    console.log('event', e)
    this.layer = Layer.MAP;
  }
  setSatellite() {
    this.layer = Layer.SATELLITE;
  }

  ngOnInit() {  }

  // get name() {
  //   // console.log('name')
  //   return `${this.firsttname} ${this.lastname}`;
  // }

  // get firsttname() {
  //   return this._firstname;
  // }
  // set firsttname(newname) {
  //   this._firstname = newname;
  // }

  // get lastname() {
  //   return 'Błaszczak';
  // }

  // color = 'green';

  // config = { color: this.color };

  // visible = true;
  // selected;
  // users = [
  //   { firstname: 'Piotr', lastname: 'Blaszczak' },
  //   { firstname: 'Paweł', lastname: 'Blaszczak' },
  //   { firstname: 'Jurek', lastname: 'Blaszczak' },
  // ];

  // counter = 0;

  // birthday = new Date().getTime();

  // title = `Witaj
  // ${this.name}
  // na
  // stronie głównej`;

  // private _firstname = 'Piotr';

  // updateTitle() {
  //   // this.visible = !this.visible;
  //   // this.color = 'red';
  //   // this.counter++;
  //   this.users.push({
  //     firstname: 'Waldek',
  //     lastname: 'Kiepski',
  //   });
  // }

}
