import { NgModule, APP_INITIALIZER } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuComponent } from './components/menu/menu.component';
import { FooterComponent } from './components/footer/footer.component';
import { HomeComponent } from './components/home/home.component';
import { ConfigService } from './config.srevice';
import { MapComponent } from './map/map.component';

@NgModule({
  declarations: [MenuComponent, FooterComponent, HomeComponent, MapComponent],
  imports: [
    CommonModule
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: (configService: ConfigService) => {
        return () => {
          return fetch('/api/config')
          .then(res => res.json())
          .catch(err => ({
            NO_CONFIG: true,
            PHOTOS_BASE_PATH: 'http://localhost:3000/photos/'
          }))
          .then(config => {
            Object.assign(configService, config);
          });
        };
      },
      deps: [ConfigService],
      multi: true,
    }
  ],
  exports: [MenuComponent, FooterComponent, HomeComponent]
})
export class SharedModule { }
