import { Component, OnInit, ElementRef, ViewChild, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import * as L from 'leaflet';
L.Icon.Default.imagePath = '/assets/leaflet/images/';

export interface Coords {
  lat: number;
  lng: number;
}
export enum Layer {
  MAP = 'map',
  SATELLITE = 'satellite'
}
@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit, OnChanges {

  @ViewChild('mapContainerId', { static: true })
  private mapContainer: ElementRef;

  private layer;
  private marker: L.Marker;
  private map: L.Map;

  @Input()
  coords: Coords;

  @Output()
  coordsChange = new EventEmitter<Coords>();

  @Input()
  layerType = Layer.MAP;


  ngOnInit() {
    this.map = L.map(this.mapContainer.nativeElement).setView([51.505, -0.09], 13);
    this.map.on('click', (e: L.LeafletMouseEvent) => {
      this.coordsChange.emit(e.latlng);
    });
    this.setupMarker();
    this.setupLayer();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.setupMarker();
    if (changes.layerType) {
      this.setupLayer();
    }
  }

  setupMarker() {
    if (this.marker) {
      this.map.removeLayer(this.marker);
    }
    if (this.coords && this.map) {
      this.marker = L.marker(this.coords);
      this.marker.addTo(this.map);
      this.map.panTo(this.coords);
    }
  }

  setupLayer() {
    if (this.layer) {
      this.map.removeLayer(this.layer);
    }
    if (!this.map) {
      return;
    }

    if (this.layerType === Layer.MAP) {

      this.layer = L.tileLayer('https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_all/{z}/{x}/{y}.png`', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
      });
      this.layer.addTo(this.map);
    } else {

      this.layer = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
        attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
      });
      this.layer.addTo(this.map);
    }

  }
}
