import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PhotosRoutingModule } from './photos-routing.module';
import { PhotoListComponent } from './photo-list/photo-list.component';
import { UploadComponent } from './upload/upload.component';
import { MatCardModule, MatButtonModule } from '@angular/material';
import { PhotoUrlPipe } from './pipes/photo-url.pipe';
import { PHOTOS_BASE_PATH } from './tokens';
import { environment } from '../../environments/environment';
import { ConfigService } from '../shared/config.srevice';
import { PhotoInfoComponent } from './photo-info/photo-info.component';

@NgModule({
  declarations: [PhotoListComponent, UploadComponent, PhotoUrlPipe, PhotoInfoComponent],
  imports: [
    CommonModule,
    PhotosRoutingModule,
    MatCardModule,
    MatButtonModule,
  ],
  providers: [{
    provide: PHOTOS_BASE_PATH,
    useFactory: (config: ConfigService) => {
      return config.PHOTOS_BASE_PATH;
    },
    deps: [ConfigService]
  }],
  exports: [PhotoListComponent]
})
export class PhotosModule { }
