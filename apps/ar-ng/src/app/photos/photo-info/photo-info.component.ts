import { Component, OnInit, OnDestroy } from '@angular/core';
import { PhotosService } from '../photos.service';
import { ActivatedRoute } from '@angular/router';
import { Subscription, Subject, Observable, EMPTY } from 'rxjs';
import { takeUntil, switchMap, map, catchError } from 'rxjs/operators';

@Component({
  selector: 'app-photo-info',
  templateUrl: './photo-info.component.html',
  styleUrls: ['./photo-info.component.scss']
})
export class PhotoInfoComponent implements OnInit {

  open = false;

  photo$: Observable<any>;

  constructor(
    private photosService: PhotosService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {

    this.photo$ = this.route.params.pipe(
      map(params => params.id),
      switchMap(id => {
        return this.photosService.getPhotoById(id).pipe(
          catchError(err => {
            console.log('ERR', err);
            return EMPTY;
          })
        );
      }),
    );

  }
}
