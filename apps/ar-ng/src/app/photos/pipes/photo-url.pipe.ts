import { Pipe, PipeTransform, Inject } from '@angular/core';
import { PHOTOS_BASE_PATH } from '../tokens';

@Pipe({
  name: 'photoUrl'
})
export class PhotoUrlPipe implements PipeTransform {

  constructor(@Inject(PHOTOS_BASE_PATH) private basePath: string) {}

  transform(value: any, args?: any): any {
    return this.basePath + value;
  }

}
