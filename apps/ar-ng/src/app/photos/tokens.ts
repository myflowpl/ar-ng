import { InjectionToken } from '@angular/core';

export const PHOTOS_BASE_PATH = new InjectionToken('PHOTOS_BASE_PATH');

// export const PHOTOS_BASE_PATH2 = new InjectionToken('PHOTOS_BASE_PATH');
// console.log('PHOTOS_BASE_PATH', PHOTOS_BASE_PATH === PHOTOS_BASE_PATH2);
