import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PhotoListComponent } from './photo-list/photo-list.component';
import { UploadComponent } from './upload/upload.component';
import { PhotoInfoComponent } from './photo-info/photo-info.component';

const routes: Routes = [{
  path: '',
  redirectTo: 'list',
}, {
  path: 'list',
  component: PhotoListComponent,
  children: [{
    path: 'upload',
    component: UploadComponent
  }, {
    path: 'info/:id',
    component: PhotoInfoComponent
  }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PhotosRoutingModule { }
