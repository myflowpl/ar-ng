import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map, delay } from 'rxjs/operators';
import {PhotoModel} from '@ar-ng/core';

@Injectable({
  providedIn: 'root'
})
export class PhotosService {

  constructor(private http: HttpClient) { }

  getPhotos(): Observable<any> {
    return this.http.get('/api/photos');
  }

  getPhotoById(id: string): Observable<PhotoModel> {
    return this.http.get<PhotoModel>('/api/photos/' + id);
  }
}
