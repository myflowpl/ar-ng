import { Component, OnInit, OnDestroy } from '@angular/core';
import { PhotosService } from '../photos.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-photo-list',
  templateUrl: './photo-list.component.html',
  styleUrls: ['./photo-list.component.scss']
})
export class PhotoListComponent implements OnInit, OnDestroy {

  photos: any[];
  destroy$ = new Subject();

  constructor(
    private photosService: PhotosService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.photosService.getPhotos().pipe(
      takeUntil(this.destroy$)
    ).subscribe(photos => {
      this.photos = photos;
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
