import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './shared/components/home/home.component';
import { ContactComponent } from './contact/contact.component';

const routes: Routes = [{
  path: '',
  component: HomeComponent
}, {
  path: 'contact',
  component: ContactComponent
}, {
  path: 'photos',
  loadChildren: () => import('./photos/photos.module').then(m => m.PhotosModule)
}, {
  path: 'user',
  loadChildren: () => import('./user/user.module').then(m => m.UserModule)
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
