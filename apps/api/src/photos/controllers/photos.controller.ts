import { Controller, Post, UseInterceptors, Body, UploadedFile, Get, Param, NotFoundException } from '@nestjs/common';
import { ApiConsumes, ApiImplicitFile, ApiImplicitParam } from '@nestjs/swagger';
import { PhotosService } from '../services/photos.service';
import { FileInterceptor } from '@nestjs/platform-express';
import { of } from 'rxjs';
import { delay } from 'rxjs/operators';

@Controller('photos')
export class PhotosController {
  private data = [
    {
      id: 1,
      filename: '1e924420ac94c2bcee5f3b0b13dcea01.jpg',
      description: 'GOPR9629.jpg',
    },
    {
      id: 2,
      filename: '9fa55dc785dbc8bf396a796b93fa0087.jpg',
      description: 'GOPR9629.jpg',
    },
  ];

  constructor(private photosService: PhotosService) { }

  @Get(':id')
  @ApiImplicitParam({ name: 'id' })
  async getPhoto(@Param('id') id) {
    // const data: any = await this.photosService.findAll();
    // console.log('ID', id', )

    const photo = this.data.find(p => p.id === parseInt(id, 10));

    if (!photo) { throw new NotFoundException('photo not found'); }

    return of(photo).pipe(
      delay(500),
    );
  }

  @Get()
  async getPhotos() {
    // const data = await this.photosService.findAll();
    return of(this.data).pipe(
      delay(500),
    );
  }

  @Post('upload-user-avatar')
  @UseInterceptors(FileInterceptor('file'))
  @ApiConsumes('multipart/form-data')
  @ApiImplicitFile({ name: 'file', required: true, description: 'Upload user avatar' })

  async uploadFile(@UploadedFile() file: Express.Multer.File, @Body() body) {
    const avatar = await this.photosService.create(file);

    const thumb = await this.photosService.createThumbs(avatar.photo.filename);

    return {
      avatar,
      file,
      body,
      thumb,
    };
  }
}
