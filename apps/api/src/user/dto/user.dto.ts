import { UserModel } from '../models';
import { ApiModelProperty } from '@nestjs/swagger';
import { IsString, MinLength, IsEmail } from 'class-validator';

export class UserRegisterRequestDto {
  name: string;
  email: string;
  password: string;
}

export class UserRegisterResponseDto {
  user: UserModel;
}

export class UserLoginRequestDto {
  @ApiModelProperty({
    description: 'Login w postaci adresu email',
    example: 'piotr@myflow.pl',
  })
  @IsEmail()
  email: string;
  
  @ApiModelProperty({
    description: 'Haslo, minimum 3 znaki',
    example: '123',
  })
  @MinLength(3)
  @IsString()
  password: string;
}

export class UserLoginResponseDto {
  token: string;
  user: UserModel;
}
