import { ApiModelPropertyOptional, ApiModelProperty } from '@nestjs/swagger';

export class CommentModel {
  @ApiModelPropertyOptional()
  id?: number;
  @ApiModelProperty()
  name: string;
}

export class ResDto {
  @ApiModelPropertyOptional()
  id: number;
  @ApiModelProperty()
  name: string;
  @ApiModelProperty()
  data: CommentModel;
}