import { CommentModel } from '../model/comment.model';

export class GetCommentResponseDto {
  total: number;
  data: CommentModel;
}
