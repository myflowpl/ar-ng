import { CommentModel } from '../model/comment.model';
import { ApiModelPropertyOptional, ApiModelProperty } from '@nestjs/swagger';

export class GetCommentsRequestDto {
  @ApiModelProperty()
  search: string;
  @ApiModelPropertyOptional()
  pageIndex: number;
  @ApiModelPropertyOptional()
  pageSize: number;
}

export class GetCommentsResponseDto {
  @ApiModelProperty()
  pageIndex: number;
  @ApiModelProperty()
  pageSize: number;
  @ApiModelProperty()
  total: number;
  @ApiModelProperty({isArray: true})
  data: CommentModel[];
  @ApiModelProperty()
  query: GetCommentsRequestDto;
}

export class PostCommentRequestDto {
  @ApiModelProperty()
  model: CommentModel;
}
